# Playing Card Cropper

This is a simple tool to crop playing cards from a photo. It is intended to be used with a screnshot of a card, with black border. The tool will automatically detect the cards and crop them out, filling the black border with black.

## Usage

First, you need to modify the `pcc.py` file to set the correct parameters for your image. The parameters are at the bottom of the file:

```python
INPUT_FOLDER = "/path/to/in" # Folder containing the image to crop
OUTPUT_FOLDER = "/path/to/out" # Folder to save the cropped images to
FILE_EXTENSION = "png" # File extension of the images to crop
FILE_PREFIX = "YourPrefix" # Prefix of the images to crop in the output folder
NUMBER_OF_DIGITS = 3 # Number of digits in the image names in the output folder
```

Then, run the script:

```bash
python pcc.py
```

## Limitations

The tool is not perfect, and will not work in all cases. It is intended to be used with a screenshot of a card, with black border. It may be able to work with other border colors, but it is not guaranteed. It may not work with a photo of a card, as the card may not be perfectly flat, and the perspective may be off.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
