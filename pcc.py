# Importing the libraries
import os
from PIL import Image

# Function to log a message into console
def log(message):
    print("[PCC] " + message)

# Function that loads an image and parse it to find the borders of a card
def find_borders(image):
    # Log the current image
    log("[Finding Border] Finding borders for image " + image)
    # Load the image
    img = Image.open(image)
    # Get the size of the image
    width, height = img.size
    # Get the pixels of the image
    pixels = img.load()
    # Get the borders of the card
    left = 0
    right = width
    top = 0
    bottom = height

    # Minimum size of the border to search
    min_border_size = 6

    # Maximum color value of the border
    max_color_value = 10

    # Find the left border,
    # Parsing the image from left to right at a third of the height of the image,
    # When more than min_border_size consecutive pixels are black we consider that the border is found.
    pixel_counter = 0
    heigh_to_search = height / 3
    for x in range(width):
        # If the pixel is black, if all 3 colors are lower than max_color_value
        if pixels[x, heigh_to_search][0] < max_color_value and pixels[x, heigh_to_search][1] < max_color_value and pixels[x, heigh_to_search][2] < max_color_value:
            # Increment the counter
            pixel_counter += 1
            # If we found more than min_border_size consecutive pixels
            if pixel_counter > min_border_size:
                # Remove the min_border_size last pixels
                left = x - min_border_size
                # Log the left border
                log("[Finding Border] Left border found at " + str(left) + " pixels")
                # The border is found
                break
        # If the pixel is not black
        else:
            # Reset the counter
            left = 0

    # Find the right border,
    # Parsing the image from right to left at a third of the height of the image,
    # When more than min_border_size consecutive pixels are black we consider that the border is found.
    pixel_counter = 0
    heigh_to_search = height / 3
    for x in range(width - 1, 0, -1):
        # If the pixel is black, if all 3 colors are lower than max_color_value
        if pixels[x, heigh_to_search][0] < max_color_value and pixels[x, heigh_to_search][1] < max_color_value and pixels[x, heigh_to_search][2] < max_color_value:
            # Increment the counter
            pixel_counter += 1
            # If we found more than min_border_size consecutive pixels
            if pixel_counter > min_border_size:
                # Remove the min_border_size last pixels
                right = x + min_border_size
                # Log the right border
                log("[Finding  Border] Right border found at " + str(right) + " pixels")
                # The border is found
                break
        # If the pixel is not black
        else:
            # Reset the counter
            right = width

    # Find the top border,
    # Parsing the image from top to bottom at a third of the width of the image,
    # When more than min_border_size consecutive pixels are black we consider that the border is found.
    width_to_search = width / 3
    pixel_counter = 0
    for y in range(height):
        # If the pixel is black, if all 3 colors are lower than max_color_value
        if pixels[width_to_search, y][0] < max_color_value and pixels[width_to_search, y][1] < max_color_value and pixels[width_to_search, y][2] < max_color_value:
            # Increment the counter
            pixel_counter += 1
            # If we found more than min_border_size consecutive pixels
            if pixel_counter > min_border_size:
                # Remove the min_border_size last pixels
                top = y - min_border_size
                # Log the top border
                log("[Finding Border] Top border found at " + str(top) + " pixels")
                # The border is found
                break
        # If the pixel is not black
        else:
            # Reset the counter
            top = 0

    # Find the bottom border,
    # Parsing the image from bottom to top at a quarter of the width of the image,
    # When more than min_border_size consecutive pixels are black we consider that the border is found.
    width_to_search = width / 4
    pixel_counter = 0
    for y in range(height - 1, 0, -1):
        # If the pixel is black, if all 3 colors are lower than max_color_value
        if pixels[width_to_search, y][0] < max_color_value and pixels[width_to_search, y][1] < max_color_value and pixels[width_to_search, y][2] < max_color_value:
            # Increment the counter
            pixel_counter += 1
            # If we found more than min_border_size consecutive pixels
            if pixel_counter > min_border_size:
                # Remove the min_border_size last pixels
                bottom = y + min_border_size
                # Log the bottom border
                log("[Finding Border] Bottom border found at " + str(bottom) + " pixels")
                # The border is found
                break
        # If the pixel is not black
        else:
            # reset the counter
            bottom = height

    # Return the borders of the card
    return left, right, top, bottom

# Function that crops an image and save the result in a new file in out directory
def crop_image(image, left, right, top, bottom, out):
    # Log the current image
    log("[Crop Image] Cropping image " + image + " with: left=" + str(left) + ", right=" + str(right) + ", top=" + str(top) + ", bottom=" + str(bottom))
    # Load the image
    img = Image.open(image)
    # Crop the image
    img = img.crop((left, top, right, bottom))
    log("[Crop Image] Saving image " + out)
    # Save the image
    img.save(out)

# Function that changes the 4 corners of a picture on a range to black
def change_corners(image, corner_range=70):
    # Log the current image
    log("[Corner Fix] Changing corners for image " + image + " with a range of " + str(corner_range) + " pixels")
    # Load the image
    img = Image.open(image)
    # Get the size of the image
    width, height = img.size
    # Get the pixels of the image
    pixels = img.load()
    # Pixel color tolerance
    tolerance = 10
    # Change the pixels of the corners to black, using the corner_range, only if the pixel is not already black (<tolerance)
    for x in range(corner_range):
        for y in range(corner_range):
            if x+y < corner_range-5:
                if pixels[x, y][0] > tolerance and pixels[x, y][1] > tolerance and pixels[x, y][2] > tolerance:
                    img.putpixel((x, y), (0, 0, 0))
                if pixels[width - x - 1, y][0] > tolerance and pixels[width - x - 1, y][1] > tolerance and pixels[width - x - 1, y][2] > tolerance:
                    img.putpixel((width - x - 1, y), (0, 0, 0))
                if pixels[x, height - y - 1][0] > tolerance and pixels[x, height - y - 1][1] > tolerance and pixels[x, height - y - 1][2] > tolerance:
                    img.putpixel((x, height - y - 1), (0, 0, 0))
                if pixels[width - x - 1, height - y - 1][0] > tolerance and pixels[width - x - 1, height - y - 1][1] > tolerance and pixels[width - x - 1, height - y - 1][2] > tolerance:
                    img.putpixel((width - x - 1, height - y - 1), (0, 0, 0))
    # Save the image
    img.save(image)

# Function that crops all images in a directory using find_borders and crop_image
def crop_images(directory):
    # Log the current directory
    log("Parsing directory " + directory)
    # Get all jpg files in the directory
    files = os.listdir(directory)
    # Log the number of files
    log("Found " + str(len(files)) + " files to crop")
    # Loop through all files
    for file in files:
        # Log the current file
        log("Parsing file " + file)
        # Get the borders of the card
        left, right, top, bottom = find_borders(directory + file)
        # Crop the image
        crop_image(directory + file, left, right, top, bottom, directory + file)
        # Change the corners of the image to black
        change_corners(directory + file)

# Function that list the images by file name
# Then rename the images in the directory from the oldest to the newest
# Using the file name format: "SUFFIX-XXX" where SUFFIX is a parameter and XXX is the number of the image on 3 digits
def rename_images(directory, suffix="Card", digits=3, extension="jpg"):
    # Log the current directory
    log("Parsing directory " + directory)
    # Get all files in the directory
    files = os.listdir(directory)
    # Log the number of files
    log("Found " + str(len(files)) + " files to rename")
    # List of files
    file_list = []
    # Loop through all files
    for file in files:
        # Add the file to the list
        file_list.append(file)
    # Sort the list of files
    file_list.sort()
    # Loop through all files
    for i in range(len(file_list)):
        # Final file name
        final_name = str(suffix) + "-" + str(i + 1).zfill(digits) + "." + extension
        # Log the current file
        log("Renaming " + file_list[i] + "into " + final_name)
        # Rename the file
        os.rename(directory + file_list[i], directory + final_name)

# ------------------- #
# Using the functions #
# ------------------- #

INPUT_FOLDER = "/path/to/in"
OUTPUT_FOLDER = "/path/to/out"

FILE_EXTENSION = "png"
FILE_PREFIX = "YourPrefix"
NUMBER_OF_DIGITS = 3

# Loggin application start
log("Starting script")

# Create the out directory if it doesn't exist
if not os.path.exists(OUTPUT_FOLDER):
    os.makedirs(OUTPUT_FOLDER)
# Copy all files from in directory to out directory
os.system("cp " + INPUT_FOLDER + "/* " + OUTPUT_FOLDER + "/")
# Rename all files in out directory
rename_images(OUTPUT_FOLDER, FILE_PREFIX, NUMBER_OF_DIGITS, FILE_EXTENSION)
# Crop all images in out directory
crop_images(OUTPUT_FOLDER)

# Loggin application end
log("Ending script")
